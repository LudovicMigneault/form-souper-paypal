<?php
//defined('_JEXEC') or die('Restricted Access');
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/fnPHP.php';
require_once __DIR__ . "/PaypalConstants.php";
session_start();

$paypalCredentials = "1231231234";
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<style>
    <?php include __DIR__ . "/formSouper.css";
    $diner = "tacos"; ?>
</style>
<script src="https://chefnath.ca/reservation/constjs.js"></script>
<?php

$soupers = getSouper();
$sheetConst = getSheetsConst();

$_SESSION['soupers'] = $soupers
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<div class = "container-fluid">
    <div class = "etatSouper"><p class = "text-center grandEtRouge"><?php print($sheetConst["TitreSouperOffline"]) ?></p> <p class = "text-center"><?php print($sheetConst["MessageSouperOffline"]) ?> </p></div>
    <div class = "etatDiner"> <p class = "text-center grandEtRouge"> <?php print($sheetConst["TitreDinerOffline"]) ?> </p> <p class = "text-center"> <?php print($sheetConst["MessageDinerOffline"]) ?> </p> </div>
</div>
<div class="container-fluid">
    <ul class="nav nav-pills nav-justified">
        <li class="nav-item">
            <a class="nav-link active" id="dinerTab">Diner</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="souperTab">Souper</a>
        </li>
    </ul>
</div>
<div class="container-fluid">
    <p id = "champs"> Les champs marqués d'un astérixe (*) sont obligatoires </p>
    <form method="post" class="reserv">
        <div class="formelement form-group">
            <label for="nom"> Nom *:</label>
            <input type="text" class="form-control" id="nom" label="Nom" />
            <div class="Invalide" id="nom"></div>
        </div>
        <div class="formelement form-group">

            <label for="prenom"> Prénom *:</label>
            <input type="text" class="form-control" id="prenom" />
            <div class="Invalide" id="prenom"></div>
        </div>
        <div class="formelement form-group">
            <label for="courriel"> Courriel *:</label>
            <input type="text" class="form-control" id="courriel" />
            <div class="Invalide" id="courriel"></div>
        </div>
        <div class="formelement form-group">
            <label for="telephone"> Numéro de téléphone *:</label>
            <input type="tel" class="form-control" id="telephone" />
            <div class="Invalide" id="telephone"></div>
        </div>
        <div class="pasBoite">
            <p>Vous devrez aller chercher votre souper le vendredi à l'endroit caché de Chef Nath </p>
        </div>
        <div class="formelement form-group boiteContainer">
            <p>Les diners doivent être préalablement commandés 24h à l'avance. Livraison à Chibougamau seulement.
                Contactez-moi pour toute demande spéciale.</p>
            <label for="date"> Date *:</label>
            <input type="text" class="form-control" id="date">
            <div class="Invalide" id="date"></div>
        </div>

        <div class="formelement form-group boiteContainer">

            <label for="address"> Adresse *:</label>
            <input type="text" class="form-control" id="address" />
            <div class="Invalide" id="address"></div>
        </div>
        <b> Mes produits </b>
        <div id="souper">
            <?php foreach ($soupers as $souper) {
                if (!$souper['boite']) {
                    echo "<div class = 'pasBoite'> -
            <label for=" . $souper['id'] . " id=" . $souper['id'] . ">" . $souper['nom'] . "<div class = 'prix'> " . $souper['prix'] . "</div>$ CAD </label>
            <label for=" . $souper['id'] . 'qte' . " id=" . $souper['id'] . 'qte' . ">" . 'Quantité : ' . "</label>
            <input id=" . $souper['id'] . 'qte' . " name=" . $souper['id'] . 'qte' . " type='number'  onchange = 'compter(" . count($soupers) . ")'></input><br></div>";
                }
                if ($souper['boite']) {
                    echo "
              <div class = 'boiteContainer'> -<label for=" . $souper['id'] . " id=" . $souper['id'] . ">" . $souper['nom'] . "<div class = 'prix'> " . $souper['prix'] . "</div>$ CAD </label>
              <label for=" . $souper['id'] . 'qte' . " id=" . $souper['id'] . 'qte' . ">" . 'Quantité : ' . "</label>
            <input id=" . $souper['id'] . 'qte' . " name=" . $souper['id'] . 'qte' . " type='number'  onchange = 'compter(" . count($soupers) . ")'></input><br>
            <div class = 'Invalide dinerErreur'></div>
            </div>";
                }
            } ?>

        </div>

        <div class="formelement form-group">

            <label for="notes"> Notes (allergies) :</label>
            <textarea rows = "4" class="form-control" id="notes" ></textarea>
        </div>

    </form>
</div>
<div class="apercuPrix">
    Sous total : <div id="soustotal"></div>
</div>
<div class="apercuPrix">
    TPS (5%) : <div id="tps"></div>
</div>
<div class="apercuPrix">
    TVQ (9,975%) : <div id="tvq"></div>
</div>
<div class="apercuPrix">
    Total : <div id="total"></div>
</div>

<div class="successMsg">Votre commande a bien été enregistrée! Merci! </div>
<div class="errorMsg">Une erreur est survenue lors du paiement.</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src='https://www.paypal.com/sdk/js?client-id=<?php echo CLIENT_ID() ?>&currency=CAD'>

    // Required. Replace SB_CLIENT_ID with your sandbox client ID.
</script>
<script>
    var phpParams = <?php echo json_encode($soupers); ?>;
    var sheetParams = <?php print(json_encode($sheetConst)) ?> ;
    console.log("PARAMETRES DE SHEET : ", sheetParams)
    /* ActiverDimanche: "TRUE"
FermerDiners: "FALSE"
FermerSoupers: "FALSE"
ForcerOuvert: "TRUE" */
</script>
<script src="https://chefnath.ca/reservation/formSouper.js"></script>

<div class="container" id='paypal-button-container'></div>