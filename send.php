<?php

namespace Sample\CaptureIntentExamples;

use DateTime;
use Exception;
//print('exception');
use Sample\PayPalClient;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;

//print("ordercreaterequest");
header("Access-Control-Allow-Origin: https://chefnath.ca/ always");
header("Access-Control-Allow-Origin: https://sandbox.paypal.com");
header("Access-Control-Allow-Origin: https://paypal.com");
header("Access-Control-Allow-Origin: https://google.com");
header("Access-Control-Allow-Origin: http://localhost:3000");

require __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . "/PaypalConstants.php";

/////// GLOBALS
$soupers = getSouper();
$bodyReservation = json_decode(file_get_contents('php://input'), true);
$const = getSheetsConst();
function nombreTotalDeDiner()
{
  global $soupers, $bodyReservation;
  $total = 0;
  foreach ($soupers as $cle => $valeur) {
    if ($valeur['boite']) {
      $total += $bodyReservation[$cle]['qte'];
    }
  }
  return $total;
}
if (nombreTotalDeDiner() < intval($const['Nombre minimum de diners']) && nombreTotalDeDiner() != 0) {
  die("Nombre insuffisant de lunch");
}

function enDecimale($string)
{
  $string = str_replace(',', '.', $string);
  return floatval($string);
}

/**
 * Vas chercker les valeurs de souper dans SHEETS 
 * (Devrait être appelé juste 1 fois)
 */
function getSouper()
{
  $client = new Google_Client();
  $client->setAuthConfig(__DIR__ . '/credentials.json');
  $client->setDeveloperKey("AIzaSyDhSPIj6c3UQclqyaIJlK3VJaEfvxa_qi0");
  $client->addScope(Google_Service_Drive::DRIVE);
  // Your redirect URI can be any registered URI, but in this example
  // we redirect back to this same page
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
  $client->setRedirectUri($redirect_uri);
  if (isset($_GET['code'])) {
    $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
  }
  $service = new Google_Service_Sheets($client);
  // Prints the names and majors of students in a sample spreadsheet:
  // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
  $spreadsheetId = '1nr3fNPp1sX9wn7xjnSryTBX8VnPSRcKxosAmLOZPoLA';
  $range = 'menu!A2:C20';
  $response = $service->spreadsheets_values->get($spreadsheetId, $range);
  $values = $response->getValues();
  //print(json_encode($values));
  $soupers = array();
  foreach ($values as $cle => $valeur) {
    if ($valeur && $valeur[0] && strlen($valeur[0])) {
      $soupers[$cle]['nom'] = $valeur[0];
      $soupers[$cle]['prix'] = enDecimale($valeur[1]);
      if (CHEAP_PAYMENT()) {
        $soupers[$cle]['prix'] = 0.1;
      }
      $soupers[$cle]['id'] = "plat" . ($cle + 1);
      if ($valeur[2] == 'TRUE') {
        $soupers[$cle]['boite'] = true;
      } elseif ($valeur[2] == 'FALSE') {
        $soupers[$cle]['boite'] = false;
      } else {
        die('Erreur, le champ diner? doit être TRUE ou FALSE');
      }
    }
  }
  //print(json_encode($soupers));
  return $soupers;
}

function saveReservationSheet()
{
  global $soupers;
  global $bodyReservation;
  $bodyResVal = $bodyReservation[count($bodyReservation) - 1];

  $client = new Google_Client();
  $client->setAuthConfig(__DIR__ . '/credentials.json');
  $client->setDeveloperKey("AIzaSyDhSPIj6c3UQclqyaIJlK3VJaEfvxa_qi0");
  $client->addScope(Google_Service_Drive::DRIVE);
  // Your redirect URI can be any registered URI, but in this example
  // we redirect back to this same page
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
  $client->setRedirectUri($redirect_uri);
  if (isset($_GET['code'])) {
    $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
  }
  $service = new Google_Service_Sheets($client);

  // Prints the names and majors of students in a sample spreadsheet:
  // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
  $spreadsheetId = '1nr3fNPp1sX9wn7xjnSryTBX8VnPSRcKxosAmLOZPoLA';
  $range = 'commandes!A2:K'; //!A2:C20';
  $iterator = 0;
  $cle = 0;
  foreach ($soupers as $cle => $valeur) {
    if ($bodyReservation[$cle]['qte'] != 0) {
      $values[$iterator] = [
        strval($bodyResVal['Date']),
        explode(" ",  $bodyResVal['nom'])[0],
        explode(" ",  $bodyResVal['nom'])[1],
        $soupers[$cle]['boite'] ? "diner" : "souper",
        $soupers[$cle]['id'],
        afficherItemDescription($soupers, $cle),
        $soupers[$cle]['prix'],
        $bodyReservation[$cle]["qte"],
        $soupers[$cle]['prix'] * $bodyReservation[$cle]["qte"],
        $soupers[$cle]['nom'],
        date("Y-m-d h:i:sa"),
        $bodyResVal["notes"]
      ];
      $iterator += 1;
    }
  }
  //var_dump($values);
  //$values = [["2020-08-01", "ludovic", "migneault", "diner", 4, "desc 2 souper...", "32,65"]];

  $requestBody = new Google_Service_Sheets_ValueRange([
    'values' => $values
  ]);
  $params = [
    'valueInputOption' => "RAW"
  ];
  $response = $service->spreadsheets_values->append($spreadsheetId, $range, $requestBody, $params);
}


/**
 * Vas chercker les valeurs de constantes dans SHEETS 
 * (Devrait être appelé juste 1 fois)
 */
function getSheetsConst()
{
  $client = new Google_Client();
  $client->setAuthConfig(__DIR__ . '/credentials.json');
  $client->setDeveloperKey("AIzaSyDhSPIj6c3UQclqyaIJlK3VJaEfvxa_qi0");
  $client->addScope(Google_Service_Drive::DRIVE);
  // Your redirect URI can be any registered URI, but in this example
  // we redirect back to this same page
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
  $client->setRedirectUri($redirect_uri);
  if (isset($_GET['code'])) {
    $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
  }
  $service = new Google_Service_Sheets($client);

  $spreadsheetId = '1nr3fNPp1sX9wn7xjnSryTBX8VnPSRcKxosAmLOZPoLA';
  $range = 'params!A2:D20';
  $response = $service->spreadsheets_values->get($spreadsheetId, $range);
  $values = $response->getValues();
  //print(json_encode($values));
  $sheetParams = array();
  foreach ($values as $cle => $valeur) {
    if ($valeur && $valeur[0] && strlen($valeur[0])) {
      if ($cle < 7) {
        $sheetParams[$valeur[0]] = array("souper" => $valeur[1], "diner" => $valeur[2], "dinerDispoACetteDate" => $valeur[3]);
        $sheetParams[$valeur[0]] = array_map(function ($bool) {
          if ($bool == "TRUE") {
            return true;
          }
          if ($bool == "FALSE") {
            return false;
          }
        }, $sheetParams[$valeur[0]]);
      } else {
        $sheetParams[$valeur[0]] = $valeur[1];
      }
    }
  }
  //print(json_encode($soupers));
  return $sheetParams;
}

function getWeekDay($date, $timezone)
{
  $futureDate = $date->getTimeStamp();
  return (floor((($futureDate + ($timezone * 3600)) / 86400)) + 5) % 7;
}


$json = json_decode(file_get_contents('php://input'), true);
//print(round(calcTaxes(round(calcPrix(json_decode(file_get_contents('php://input'),true)),2)),2));

//if (date('w') >= 5){
//die("Trop tard pour les commandes, revenez la semaine prochaine");
//} 

function calcPrix()
{
  global $bodyReservation, $soupers, $const;
  $bodyResVal = $bodyReservation[count($bodyReservation) - 1];

  $semaine = array(0 => 'Dimanche', 1 => 'Lundi', 2 => "Mardi", 3 => 'Mercredi', 4 => "Jeudi", 5 => "Vendredi", 6 => "Samedi");

  //$soupers = array(
  // array("nom" => "Crème de légumes verts (1L)", "prix" => 9.2, "id"=>"plat1", 'boite' => false),
  // array("nom"=> "Salade d'orzo aux fruits de mer ( 2 unités )", "prix" => 34.5, "id"=>"plat2", 'boite' => false),
  // array("nom"=> "Jarret de porc nagano braisé", "prix" => 34.5, "id"=>"plat3", 'boite' => false),
  //array("nom"=> "Truffe au chocolat maison", "prix" => 2.88, "id"=>"plat4", 'boite' => false),
  //array("nom"=> "Wrap mexicain au poulet", "prix" => 17.39, "id"=>"plat5", 'boite' => true)
  // );
  $sousTotal  = 0;
  //A VÉRIFIER SI C'EST LE BON PRIX!!!
  //a valider si le calcul des taxes se fait comme il faut
  //print(json_encode($req));
  foreach ($bodyReservation as $cle => $plat) {
    if ($cle <= count($soupers) - 1) {
      $prix = ($soupers[$cle]['prix']);
      //$prix = "hello";
      if ($prix < 0 || empty($plat['prix'])) {
        $prix = 0;
      }
    


    //print("passé try catch du prix");
    $quantite = (int) $plat['qte'];
    if ($quantite < 0) {
      die("Erreur, aucune quantitée ne doit être négative");
    }
    //$quantite = "hello";
    if ($quantite < 0 || empty($plat['qte'])) {

      $quantite = 0;
      //throw new Exception("La quantité ne peut être négative");
    }
  
    if (!$const[$semaine[date('w')]]['souper'] && !$soupers[$cle]['boite'] && $quantite != 0) {
      die("les soupers ne sont pas disponibles");
    }
    $commandeDate = date_create($bodyResVal['Date']);
    //print(getWeekDay($commandeDate, -5). "jour");
    //print($commandeDate->format("yyyy-m-d"));
    if ($soupers[$cle]['boite'] && $quantite != 0 && !$const[$semaine[getWeekDay($commandeDate, -5)]]['dinerDispoACetteDate']) {
      print(json_encode($const));
      if ($const[$semaine[date('w', $commandeDate)]]['dinerDispoACetteDate']) {
        //print("diners dispo a cette date");
      }
      if (!$const[$semaine[date('w', $commandeDate)]]['dinerDispoACetteDate']) {
        print("diners pas dispo a cette date");
        print(getWeekDay($commandeDate, -6) . "jour");
      }
      die("les diners ne sont pas disponibles");
    }
    if (!$const[$semaine[date('w')]]['diner'] && $soupers[$cle]['boite'] && $quantite != 0) {
      die("les diners ne sont pas disponibles");
    }
    $sousTotal += floatval($prix * $quantite);
  }
}
  return $sousTotal;
}

function calcTaxes($sousTotal)
{
  return (0.09975 * $sousTotal + 0.05 * $sousTotal);
}

function nourritureCommandee()
{
  global $bodyReservation, $soupers;

  $listeNourriture = array();
  $it = 0;
  $fin = count($bodyReservation) - 1;
  foreach ($bodyReservation as $cle => $valeur) {
    if ($cle <= count($soupers) - 1) {
      if ($bodyReservation[$cle]['qte'] != 0) {
        $listeNourriture[$it] = array(
          'name' => $soupers[$cle]["nom"],
          "sku" => $soupers[$cle]['id'],
          "description" => afficherItemDescription($soupers, $cle),
          'unit_amount' => array("currency_code" => "CAD", "value" => strval(number_format(round($soupers[$cle]['prix'], 2), 2))),
          //"tax" => array('currency_code' => "CAD", "value" => strval(number_format(calcTaxes($soupers[$cle]['prix']),2))),
          "quantity" => strval($bodyReservation[$cle]['qte']),
          "category" => "PHYSICAL_GOODS"
        );
        $it += 1;
      }
    }
  }
  return $listeNourriture;
}
//print(json_encode(nourritureCommandee()));
//print(json_encode(nourritureCommandee()));
function afficherItemDescription($soupers, $cle)
{
  global $bodyReservation, $soupers;
  $bodyResVal = $bodyReservation[count($bodyReservation) - 1];

  $ret = "";
  $noteMsg = "";
  if ($bodyResVal['notes'] |= null && strlen($bodyResVal['notes']) > 0) {
    $noteMsg = ", notes : " . $bodyResVal["notes"];
  }
  if ($soupers[$cle]['boite']) {
    $ret = "#" . $bodyReservation[$cle]['id'] . " :À délivrer le " . strval($bodyResVal['Date']) . $noteMsg;
  } else {
    $ret = "#" . $bodyReservation[$cle]['id'] . " :À venir chercher le vendredi à l'endroit caché";
  }

  

  return $ret . $noteMsg;
}
function addresse()
{
  global $bodyReservation, $soupers;
  $bodyResVal = $bodyReservation[count($bodyReservation) - 1];

  return array(
    'method' => 'Aucune',
    'address' =>
    array(
      'address_line_1' => $bodyResVal['Adresse'],
      'admin_area_1' => "QC",
      "admin_area_2" => "Chibougamau",
      "postal_code" => "XXX XXX",
      'country_code' => "CA"
    )
  );
}

class CreateOrder
{

  // 2. Set up your server to receive a call from the client
  /**
   *This is the sample function to create an order. It uses the
   *JSON body returned by buildRequestBody() to create an order.
   */
  public static function createOrder($debug = false)
  {
    $request = new OrdersCreateRequest();
    $request->prefer('return=representation');
    $request->body = self::buildRequestBody();

    //print (json_encode($request->body));

    // 3. Call PayPal to set up a transaction
    $client = PayPalClient::client();
    $response = $client->execute($request);
    echo json_encode($response->result, JSON_PRETTY_PRINT);
    //header('Location :http://localhost:3000/sucess.php');
    if ($debug) {
      print "Status Code: {$response->statusCode}\n";
      print "Status: {$response->result->status}\n";
      print "Order ID: {$response->result->id}\n";
      print "Intent: {$response->result->intent}\n";
      print "Links:\n";
      foreach ($response->result->links as $link) {
        print "\t{$link->rel}: {$link->href}\tCall Type: {$link->method}\n";
      }

      // To print the whole response body, uncomment the following line
      echo json_encode($response->result, JSON_PRETTY_PRINT);
    }

    try {
      //saveReservationSheet();
    } catch (Exception $e) {
      echo 'Message: ' . $e->getMessage();
    }
    // 4. Return a successful response to the client.
    return $response;
  }

  /**
   * Setting up the JSON request body for creating the order with minimum request body. The intent in the
   * request body should be "AUTHORIZE" for authorize intent flow.
   *
   */
  private static function buildRequestBody()
  {
    global $bodyReservation, $soupers;

    $prix = calcPrix($bodyReservation);

    $contientDiner = false;
    foreach ($soupers as $cle => $value) {
      if ($bodyReservation[$cle]['qte'] != 0) {
        if ($value['boite']) {
          $contientDiner = true;
          break;
        }
      }
    }

    $commande =
      array(
        'intent' => 'CAPTURE',
        'application_context' =>
        array(
          'return_url' => 'http://chefnath.ca/reservation/succes.php',
          'cancel_url' => 'http://chefnath.ca/reservation/cancel.php'
        ),
        'purchase_units' =>
        array(
          0 =>
          array(
            'amount' =>
            array(
              'currency_code' => "CAD",
              'value' => number_format(round($prix, 2) + round(calcTaxes(round($prix, 2)), 2), 2),
              'breakdown' =>
              array(
                'item_total' =>
                array(
                  'currency_code' => "CAD",
                  'value' => number_format($prix, 2),
                ),
                'tax_total' =>
                array(
                  'currency_code' => "CAD",
                  'value' => number_format(calcTaxes(round($prix, 2)), 2),
                )
              )
            ),
            'items' => nourritureCommandee()
          )
        )
      );

    if ($contientDiner) {
      $commande['purchase_units'][0]['shipping'] = addresse();
    }
    //print(json_encode($commande));
    return $commande;
  }
}



/**
 *This is the driver function that invokes the createOrder function to create
 *a sample order.
 */

CreateOrder::createOrder(false);
