<?php

namespace Sample;

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\PayPalEnvironment;
use PayPalCheckoutSdk\Core\ProductionEnvironment;

require_once __DIR__ . "/PaypalConstants.php";

ini_set('error_reporting', E_ALL); // or error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');

class PayPalClient
{
    /**
     * Returns PayPal HTTP client instance with environment that has access
     * credentials context. Use this instance to invoke PayPal APIs, provided the
     * credentials have access.
     */
    public static function client()
    {
        return new PayPalHttpClient(self::environment());
    }

    /**
     * Set up and return PayPal PHP SDK environment with PayPal access credentials.
     * This sample uses LiveEnvironment. In production, use LiveEnvironment.
     */
    public static function environment()
    {
        /*
Nath LIVE : AUqOz9CFSukH9wWUrW-QwVjCfSS5fDUGRj78uC33fLA6THXh4_jrhFMf9_UpoRgyaKTHGDQz0eLHzXOf
Ludov LIVE : AUV3YOfM10i-V4SrxvEMu8z0prHmn3DqFgozvsfKrbJpy59IqcDC0EZzTN7WzUGOKKuicfoqqfovdtJQ
Ludo Secret LIVE : EDfgq6E7UhSLGLd6YYVZ9_xDjc0BSWHrlwv_3ByNgWe45hTe8ywWl3GPJ9Z0ZaidFiNb8dyxFU_GrFUD
NATH SECRET EJPWl9vMbXtKdVUZOwTeJqitMpDduPQ77rpeWnP5iyXrPHClhnX2W-o2NWnajJ0nZtsnsaBn2sqkRO2h
SANDBOX : AXCaj6PlFvijgsPxzp-wnvERbNPxMfvdwawKt54Ih8nkzaLS1tWDUkXP3W-EM_BfyDqFxN7Kq_5bQ8W8
*/
        return new ProductionEnvironment(CLIENT_ID(), CLIENT_SECRET());
    }
}
