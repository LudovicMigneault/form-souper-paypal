<?php

namespace Sample\CaptureIntentExamples;

use DateTime;
use Exception;
//print('exception');
use Sample\PayPalClient;
//print("paypal client");
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;

//print("ordercreaterequest");
header("Access-Control-Allow-Origin: https://chefnath.ca/reservation/formulaireSouper.php always");
header("Access-Control-Allow-Origin: https://chefnath.ca/ always");
header("Access-Control-Allow-Origin: https://sandbox.paypal.com");
header("Access-Control-Allow-Origin: https://paypal.com");
header("Access-Control-Allow-Origin: https://google.com");

require __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . "/PaypalConstants.php";

$soupers = getSouper();
$bodyReservation = json_decode(file_get_contents('php://input'), true);


function enDecimale($string)
{
  $string = str_replace(',', '.', $string);
  return floatval($string);
}
/**
 * Vas chercker les valeurs de souper dans SHEETS 
 * (Devrait être appelé juste 1 fois)
 */
function getSouper()
{
  $client = new Google_Client();
  $client->setAuthConfig(__DIR__ . '/credentials.json');
  $client->setDeveloperKey("AIzaSyDhSPIj6c3UQclqyaIJlK3VJaEfvxa_qi0");
  $client->addScope(Google_Service_Drive::DRIVE);
  // Your redirect URI can be any registered URI, but in this example
  // we redirect back to this same page
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
  $client->setRedirectUri($redirect_uri);
  if (isset($_GET['code'])) {
    $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
  }
  $service = new Google_Service_Sheets($client);
  // Prints the names and majors of students in a sample spreadsheet:
  // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
  $spreadsheetId = '1nr3fNPp1sX9wn7xjnSryTBX8VnPSRcKxosAmLOZPoLA';
  $range = 'menu!A2:C20';
  $response = $service->spreadsheets_values->get($spreadsheetId, $range);
  $values = $response->getValues();
  //print(json_encode($values));
  $soupers = array();
  foreach ($values as $cle => $valeur) {
    if ($valeur && $valeur[0] && strlen($valeur[0])) {
      $soupers[$cle]['nom'] = $valeur[0];
      $soupers[$cle]['prix'] = enDecimale($valeur[1]);
      if(USE_SANDBOX()){
        $soupers[$cle]['prix'] = 0.1;
      }
      $soupers[$cle]['id'] = "plat" . ($cle + 1);
      if ($valeur[2] == 'TRUE') {
        $soupers[$cle]['boite'] = true;
      } elseif ($valeur[2] == 'FALSE') {
        $soupers[$cle]['boite'] = false;
      } else {
        die('Erreur, le champ diner? doit être TRUE ou FALSE');
      }
    }
  }
  //print(json_encode($soupers));
  return $soupers;
}

function afficherItemDescription( $cle)
{
  global $bodyReservation, $soupers;

  $reqFin = count($bodyReservation) - 1;
  if ($soupers[$cle]['boite']) {
    return "#" . $bodyReservation[$cle]['id'] . " :À délivrer le " . strval($bodyReservation[$reqFin - 1]['Date']);
  } else {
    return "#" . $bodyReservation[$cle]['id'] . " :À venir chercher le vendredi à l'endroit caché";
  }
}

function saveReservationSheet()
{
  global $soupers;
  global $bodyReservation;
  $bodyResVal = $bodyReservation[count($bodyReservation) - 1];

  $client = new Google_Client();
  $client->setAuthConfig(__DIR__ . '/credentials.json');
  $client->setDeveloperKey("AIzaSyDhSPIj6c3UQclqyaIJlK3VJaEfvxa_qi0");
  $client->addScope(Google_Service_Drive::DRIVE);
  // Your redirect URI can be any registered URI, but in this example
  // we redirect back to this same page
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
  $client->setRedirectUri($redirect_uri);
  if (isset($_GET['code'])) {
    $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
  }
  $service = new Google_Service_Sheets($client);

  // Prints the names and majors of students in a sample spreadsheet:
  // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
  $spreadsheetId = '1nr3fNPp1sX9wn7xjnSryTBX8VnPSRcKxosAmLOZPoLA';
  $range = 'commandes!A2:R'; //!A2:C20';
  $iterator = 0;
  $cle = 0;
  foreach ($soupers as $cle => $valeur) {
    if ($bodyReservation[$cle]['qte'] != 0) {
      $values[$iterator] = [
        strval($bodyResVal['Date']),
        explode(" ", $bodyResVal['nom'])[0],
        explode(" ", $bodyResVal['nom'])[1],
        $valeur['boite'] ? "diner" : "souper",
        $valeur['id'],
        afficherItemDescription( $cle),
        $valeur['prix'],
        $bodyReservation[$cle]["qte"],
        $valeur['prix'] * $bodyReservation[$cle]["qte"],
        $valeur['nom'],
        date("Y-m-d h:i:sa"),
        $bodyResVal["notes"]
      ];
      $iterator += 1;
    }
  }
  //var_dump($values);
  //$values = [["2020-08-01", "ludovic5", "test", "diner", 4, "desc 2 souper...", "32,65"]];

  //print(json_encode($values));
  $requestBody = new Google_Service_Sheets_ValueRange([
    'values' => $values
  ]);
  $params = [
    'valueInputOption' => "RAW"
  ];
  $service->spreadsheets_values->append($spreadsheetId, $range, $requestBody, $params);
}


try {
    saveReservationSheet();
  } catch (Exception $e) {
    echo 'Message: ' . $e->getMessage();
  }

?>
Merci! Votre transaction a été complétée avec succès. La facture devrait être disponible dans votre compte paypal 
