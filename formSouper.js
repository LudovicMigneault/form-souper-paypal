console.log("JS - form souper");

jQuery(".successMsg").hide();
jQuery(".errorMsg").hide();

function ludoString(id) {
  let val = $("input#" + id)
    .val();
  return val == null ? "" : val.toString() || "";
}

var semaine = [
  "Dimanche",
  "Lundi",
  "Mardi",
  "Mercredi",
  "Jeudi",
  "Vendredi",
  "Samedi",
];
$(function () {
  $("#date").datepicker({
    altField: "#datepicker",
    closeText: "Fermer",
    prevText: "Précédent",
    nextText: "Suivant",
    currentText: "Aujourd'hui",
    monthNames: [
      "Janvier",
      "Février",
      "Mars",
      "Avril",
      "Mai",
      "Juin",
      "Juillet",
      "Août",
      "Septembre",
      "Octobre",
      "Novembre",
      "Décembre",
    ],
    monthNamesShort: [
      "Janv.",
      "Févr.",
      "Mars",
      "Avril",
      "Mai",
      "Juin",
      "Juil.",
      "Août",
      "Sept.",
      "Oct.",
      "Nov.",
      "Déc.",
    ],
    dayNames: [
      "Dimanche",
      "Lundi",
      "Mardi",
      "Mercredi",
      "Jeudi",
      "Vendredi",
      "Samedi",
    ],
    dayNamesShort: ["Dim.", "Lun.", "Mar.", "Mer.", "Jeu.", "Ven.", "Sam."],
    dayNamesMin: ["D", "L", "M", "M", "J", "V", "S"],
    weekHeader: "Sem.",
    dateFormat: "yy-mm-dd",
    firstDay: 1,
    minDate: dateMinimumCommande(),
    maxDate: "+" + 30 + "d",
  });
  var today = new Date().getDay();
  function dateMinimumCommande() {
    var hour = new Date().getHours();
    return hour >= 12 ? "+2d" : "+1d";
  }
  $("#date").datepicker("option", "beforeShowDay", function (date) {
    var td = date.getDay();
    var dispo = [
      sheetParams[semaine[td]]["dinerDispoACetteDate"],
      "",
      "Non disponible pour cette journée",
    ];
    return dispo;
  });
  // pas de soupers

  if (!sheetParams[semaine[today]]["souper"]) {
    $(".pasBoite input").prop("disabled", true);
    $("#souperTab").hide();
  }
  if (sheetParams[semaine[today]]["souper"]) {
    $(".etatSouper").hide();
  }
  if (!sheetParams[semaine[today]]["diner"]) {
    $(".boiteContainer input").prop("disabled", true);
    $("#dinerTab").hide();
  }
  if (sheetParams[semaine[today]]["diner"]) {
    $(".etatDiner").hide();
  }
  if (
    !sheetParams[semaine[today]]["diner"] &&
    !sheetParams[semaine[today]]["souper"]
  ) {
    $(
      "form.reserv, .apercuPrix, div#paypal-button-container,.nav, #champs"
    ).hide();
  }

  $(".pasBoite").hide();
});

$("#souperTab").click(() => {
  $(".pasBoite").show();
  $(".boiteContainer").hide();
  $("#souperTab").addClass("active");
  $("#dinerTab").removeClass("active");
});
$("#dinerTab").click(function () {
  $(".pasBoite").hide();
  $(".boiteContainer").show();
  $("#dinerTab").addClass("active");
  $("#souperTab").removeClass("active");
});
$("input#date").change(() => {
  var today = new Date();
  var todayWeekDay = today.getDay();
  var dateCommande = new Date($("input#date").val());
  var jourSemaineCommande = new Date($("input#date").val()).getDay();
  console.log(
    "today: " +
      todayWeekDay +
      "jourSemaineCommande: " +
      jourSemaineCommande +
      "difference :" +
      differenceInDays(today, dateCommande)
  );
  if (
    todayWeekDay >= jourSemaineCommande ||
    differenceInDays(dateCommande, today) >= 7
  ) {
    $("div.Invalide#date").html(
      "Attention, le repas que vous aurez sera différent que celui que vous commandez car les plats changent à chaque semaine"
    );
    console.log("invalide date");
  }
});
function differenceInDays(date1, date2) {
  console.log("date1: " + date1.getTime() + "date 2:" + date2.getTime());
  return Math.abs(Math.trunc((date1.getTime() - date2.getTime()) / 86400000));
}
function vendrediProchain() {
  var today = new Date().getDay();
  var joursRestants = 5 - today;
  if (joursRestants < 0) {
    joursRestants = 0;
  }
  return joursRestants.toString();
}

function compter(longueur, souper) {
  var pretotal = 0.0;
  for (i = 1; i <= longueur; i++) {
    if (Number($("input" + "#plat" + i + "qte").val()) < 0) {
      Number($("input" + "#plat" + i + "qte").val(0));
    }
    pretotal +=
      Number($("input" + "#plat" + i + "qte").val()) *
      parseFloat($("#plat" + i + " .prix").text());
  }
  $("#soustotal").html(pretotal.toFixed(2) + "$ CAD");
  var tps = 0.05 * pretotal;
  $("#tps").html(tps.toFixed(2) + "$CAD");
  var tvq = 0.09975 * pretotal;
  $("#tvq").html(tvq.toFixed(2) + "$CAD");
  var total = pretotal + tps + tvq;
  $("#total").html(total.toFixed(2) + "$CAD");
}
function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
}
function tropLong(obj) {
  if (obj.length > 100) {
    return true;
  }
  return false;
}
function validerIntrants() {
  jQuery(".errorMsg").hide();
  if ($("#dinerTab").hasClass("active")) {
    var idRangee = [
      "nom",
      "prenom",
      "courriel",
      "telephone",
      "date",
      "address",
      "notes",
    ];
  } else if ($("#souperTab").hasClass("active")) {
    var idRangee = ["nom", "prenom", "courriel", "telephone", "notes"];
  }
  idRangee.map((id) => {
    if (
      id != "notes" &&
      isEmpty(
        ludoString(id)
      )
    ) {
      $("div.Invalide#" + id).html("Vous devez entrer ce paramètre");
      showError("Révisez le formulaire, certains champs sont mal saisis.");
      throw new Error("Certains parmamètres sont manquants");
    }
    if (
      id != "notes" &&
      tropLong(
        ludoString(id)
      )
    ) {
      $("div.Invalide#" + id).html(
        "La longueur de ce qu'il y a dans ce champ est trop grande"
      );
      showError("Révisez le formulaire, certains champs sont mal saisis.");
      throw new Error("troplong");
    } else {
      $("div.Invalide#" + id).html("");
    }
  });
  if (
    nombreTotalDeDiner() < Number(sheetParams["Nombre minimum de diners"]) &&
    $("div.boiteContainer input[type = 'number']").val() != 0
  ) {
    $("div.boiteContainer div.Invalide.dinerErreur").html(
      "Vous devez soit prendre au moins " +
        sheetParams["Nombre minimum de diners"] +
        " boites à lunch, ou ne pas en prendre"
    );
    showError("Révisez le formulaire, certains champs sont mal saisi.");
    throw new Error("nombre de lunch incorrect");
  } else {
    $("div.boiteContainer div.Invalide.dinerErreur").html("");
  }
}
function nombreTotalDeDiner() {
  total = 0;
  $("div.boiteContainer input[type = 'number']").each((index, elem) => {
    total += Number($(elem).val());
    console.log(total);
  });
  return total;
}
function callServer(url, msgErreur) {
  return fetch(url, {
    method: "post",
    headers: {
      "content-type": "application/json",
      "User-Agent": "ABCDEFGHIJ"
    },
    body:
      //phpParams.map(menuItem=>({id: menuItem.id, qte:1, prix: menuItem.prix}))
      JSON.stringify(
        phpParams
          .map((menuItem) => ({
            id: menuItem.id,
            qte: Number($("input#" + menuItem.id + "qte").val()),
            prix: menuItem.prix,
          }))
          .concat([
            {
              nom: (
                $("input#prenom").val() +
                " " +
                $("input#nom").val()
              ).toString(),
              courriel: ludoString("courriel"),
              "Numéro de téléphone": ludoString("telephone"),
              Date: ludoString("date"),
              Adresse: ludoString("address"),
              notes: ludoString("notes"),
              erreur: msgErreur,
            },
          ])
      ),
  });
}

function showSuccess() {
  jQuery(".successMsg").show();
  jQuery(".successMsg").addClass("active");
}

function showError(errMsg) {
  jQuery(".errorMsg").show();
  jQuery(".errorMsg").addClass("active");
  if(errMsg){
    jQuery(".errorMsg").text(errMsg);
  }else{
    jQuery(".errorMsg").text("Une erreur est survenue lors du paiement.");
  }
}

paypal
  .Buttons({
    createOrder: function (data, actions) {
      validerIntrants();
      return callServer(URL_PATH + "/send.php")
        .then(function (res) {
          console.log("thenres");
          //console.log(res.json());
          return res.json();
        })
        .then(function (data) {
          console.log("thendata");
          return data["id"];
          //return data.orderID; // Use the same key name for order ID on the client and server
        })
        .catch((err) => {
          console.log("erreur", err.toString());
          showError(err);
          callServer(URL_PATH + "/logerreur.php", err.toString());
        });
    },

    onApprove: async (data, actions) => {
      try {
        const order = await actions.order.capture();
        console.log("Order complete ", order);
        callServer(URL_PATH + "/succes.php", order);
        showSuccess();
        jQuery(document).scrollTop(0);
      } catch (error) {
        console.log("erreur", error.toString());
        showError(err);
        callServer(URL_PATH + "/logerreur.php", err.toString());
      }
    },
    onError: (err) => {
      callServer(URL_PATH + "/cancel.php");
      window.location.href(URL_PATH + "/erreur.html");
      console.error(err);
      showError(err);
      callServer(URL_PATH + "/logerreur.php", err.toString());
      jQuery(document).scrollTop(0);
    },
  })
  .render("#paypal-button-container");
//This function displays Smart Payment Buttons on your web page.
