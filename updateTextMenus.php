<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/fnPHP.php';

if (!isset($soupers)) {
    $soupers = getSouper();
}
if (!isset($sheetConst)) {
    $sheetConst = getSheetsConst();
}

?>

<script>
    const p = inner => inner ? ("<p>" + inner + "</p>") : "";
    const strong = inner => inner ? ("<strong>" + inner + "</strong>") : "";
    const withBR = text => text ? text.replace(/(?:\r\n|\r|\n)/g, '<br/>') : "";

    var phpParams = <?php echo json_encode($soupers); ?>;
    var sheetParams = <?php print(json_encode($sheetConst)) ?>;

    const createMenuHTML = (className, filter) => (
        jQuery("."+className+" p:last-child").parent().html(
            phpParams.filter(filter).map(
                r => p(strong(r.nom + " | " + r.prix + "$")) +
                p(withBR(r.desc))
            ).join("<br/>")
        )
    )

    //console.log("PARAMETRES DE SHEET : ", sheetParams)
    //console.log("PARAMETRES DE Souper : ", phpParams)

    createMenuHTML("menuDiner", r=>r.boite);
    createMenuHTML("menuSouper", r=>!r.boite);
    
    /* Ex : <p><strong>BAGUETTE À LA SALADE DE POULET À LA MOUTARDE | 17.39$</strong></p>
<p class="desc">- Salade de pâtes</p>*/

</script>