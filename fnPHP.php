<?php

require_once __DIR__ . "/PaypalConstants.php";

function enDecimale($string)
{
    $string = str_replace(',', '.', $string);
    return floatval($string);
}

function utf8_encode_deep(&$input)
{
  if (is_string($input)) {
    $input = utf8_encode($input);
  } else if (is_array($input)) {
    foreach ($input as &$value) {
      utf8_encode_deep($value);
    }

    unset($value);
  } else if (is_object($input)) {
    $vars = array_keys(get_object_vars($input));

    foreach ($vars as $var) {
      utf8_encode_deep($input->$var);
    }
  }
}

function getSouper()
{
  $client = new Google_Client();
  $client->setAuthConfig(__DIR__ . '/credentials.json');
  $client->setDeveloperKey("AIzaSyDhSPIj6c3UQclqyaIJlK3VJaEfvxa_qi0");
  $client->addScope(Google_Service_Drive::DRIVE);
  // Your redirect URI can be any registered URI, but in this example
  // we redirect back to this same page
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
  $client->setRedirectUri($redirect_uri);
  if (isset($_GET['code'])) {
    $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
  }
  $service = new Google_Service_Sheets($client);

  // Prints the names and majors of students in a sample spreadsheet:
  // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
  $spreadsheetId = '1nr3fNPp1sX9wn7xjnSryTBX8VnPSRcKxosAmLOZPoLA';
  $range = 'menu!A2:D20';
  $response = $service->spreadsheets_values->get($spreadsheetId, $range);
  $values = $response->getValues();
  //print(json_encode($values));
  $soupers = array();
  foreach ($values as $cle => $valeur) {
    if ($valeur && $valeur[0] && strlen($valeur[0])) {
      $soupers[$cle]['nom'] = $valeur[0];
      $soupers[$cle]['prix'] = enDecimale($valeur[1]);
      if(USE_SANDBOX()){
        $soupers[$cle]['prix'] = 0.1;
      }
      $soupers[$cle]['id'] = "plat" . ($cle + 1);

      $soupers[$cle]['boite'] = ($valeur[2] == 'TRUE');

      // else {
      //  die('Erreur, le champ diner? doit être TRUE ou FALSE');
      // }

      $soupers[$cle]["desc"] = $valeur[3];
    }
  }
  //print(json_encode($soupers));
  return $soupers;
}
function getSheetsConst()
{
  $client = new Google_Client();
  $client->setAuthConfig(__DIR__ . '/credentials.json');
  $client->setDeveloperKey("AIzaSyDhSPIj6c3UQclqyaIJlK3VJaEfvxa_qi0");
  $client->addScope(Google_Service_Drive::DRIVE);
  // Your redirect URI can be any registered URI, but in this example
  // we redirect back to this same page
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
  $client->setRedirectUri($redirect_uri);
  if (isset($_GET['code'])) {
    $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
  }
  $service = new Google_Service_Sheets($client);

  $spreadsheetId = '1nr3fNPp1sX9wn7xjnSryTBX8VnPSRcKxosAmLOZPoLA';
  $range = 'params!A2:D20';
  $response = $service->spreadsheets_values->get($spreadsheetId, $range);
  $values = $response->getValues();
  //print(json_encode($values));
  $sheetParams = array();
  foreach ($values as $cle => $valeur) {
    if ($valeur && $valeur[0] && strlen($valeur[0])) { /// <<-- Ajouter cette ligne dans getSouper pour avoir un nombre variable de repas ( Et lire les ranges plus loin )
      if ($cle < 7){
      $sheetParams[$valeur[0]] = array("souper" => $valeur[1], "diner" => $valeur[2], "dinerDispoACetteDate" => $valeur[3]);
      $sheetParams[$valeur[0]] = array_map(function ($bool) {
        if ($bool == "TRUE") {
          return true;
        }
        if ($bool == "FALSE") {
          return false;
        }
      }, $sheetParams[$valeur[0]]);
  }
  else{
    $sheetParams[$valeur[0]] = $valeur[1];
  }
  }
  }
  //print(json_encode($soupers));
  return $sheetParams;
}
